//jigsaw
#include "jigsawcalculator/JigsawCalculator.h"
#include "jigsawcalculator/utility/helpers.h"
#include "jigsawcalculator/JigsawType.h"
#include "jigsawcalculator/calculators/Calculators.h"

//std/stl
#include <iostream>
using namespace std;

namespace jigsaw
{

JigsawCalculator::JigsawCalculator()
{
}

bool JigsawCalculator::initialize(std::string jigsaw_type)
{
    cout << JIGFUNC << "   Initializing JigsawType: " << jigsaw_type << endl;
    if(!IsValidJigsawType(jigsaw_type))
    {
        cout << JIGFUNC << "   ERROR Invalid JigsawType provided (=" << jigsaw_type << ")" << endl;
        cout << JIGFUNC << "   ERROR  > valid JigsawTypes are: ";
        for(auto t : ValidJigsawTypes()) cout << t << ", ";
        cout << endl;
        return false;
    }

    m_jigsaw_type = jigsaw_type;

    if(JigsawTypeFromStr(jigsaw_type) == JigsawType::TTMET2LW)
    {
        m_imp = std::make_unique<jigsaw::CalculatorTTMET2LW>();
    }
    else
    {
        cout << JIGFUNC << "    ERROR There is no support for the requested JigsawType (=" << jigsaw_type << ")" << endl;
        return false;
    }

    try
    {
        m_imp->initialize_calculator();
    }
    catch(std::exception& e)
    {
        cout << JIGFUNC << "    ERROR JigsawCalculator failed to initialize Jigsaw tree: " << e.what() << endl;
        return false;
    }

    return true;
}

bool JigsawCalculator::load_event(std::map<std::string, std::vector<TLorentzVector> > object_map)
{
    return m_imp->load_event(object_map);
}

std::string JigsawCalculator::jigsaw_type()
{
    return m_jigsaw_type;
}

std::map<string, float> JigsawCalculator::variables()
{
    return m_imp->variables_map();
}

std::vector<std::string> JigsawCalculator::variable_names()
{
    return m_imp->variable_names();
}

} // namespace jigsaw
