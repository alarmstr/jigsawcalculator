#ifndef JIGSAW_HELPERS
#define JIGSAW_HELPERS

#include <string>

std::string computeMethodName(const std::string& function, const std::string& prettyFunction);
#define JIGFUNC computeMethodName(__FUNCTION__,__PRETTY_FUNCTION__).c_str()

#endif
