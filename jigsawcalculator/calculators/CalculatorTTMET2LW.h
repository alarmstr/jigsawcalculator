#ifndef JIGSAW_CALC_TTMET2LW_H
#define JIGSAW_CALC_TTMET2LW_H

//jigsaw
#include "jigsawcalculator/JigsawCalculatorImp.h"

//std/stl
#include <string>
#include <vector>
#include <map>
#include <memory>

#include "RestFrames/RestFrames.hh"

//RestFrames
namespace RestFrames
{
    class LabRecoFrame;
    class DecayRecoFrame;
    class VisibleRecoFrame;
    class InvisibleRecoFrame;
    class InvisibleGroup;
    class CombinatoricGroup;
    class SetMassInvJigsaw;
    class SetRapidityInvJigsaw;
    class ContraBoostInvJigsaw;
    class MinMassesCombJigsaw;
}

namespace jigsaw
{

class CalculatorTTMET2LW : public JigsawCalculatorImp
{

    public :
        explicit CalculatorTTMET2LW();
        std::string type();
        void initialize_calculator(); // build the Jigsaw tree
        bool load_event(std::map<std::string, std::vector<TLorentzVector>> object_map);
        void add_var(std::string name, float val);
        void fill_variables();
        std::map<std::string, float> variables_map();
        std::vector<std::string> variable_names();

    private :

        //RestFrames
        std::unique_ptr<RestFrames::LabRecoFrame> rf_lab;
        std::unique_ptr<RestFrames::DecayRecoFrame> rf_ss;
        std::unique_ptr<RestFrames::DecayRecoFrame> rf_s1;
        std::unique_ptr<RestFrames::DecayRecoFrame> rf_s2;
        std::unique_ptr<RestFrames::VisibleRecoFrame> rf_v1;
        std::unique_ptr<RestFrames::VisibleRecoFrame> rf_v2;
        std::unique_ptr<RestFrames::InvisibleRecoFrame> rf_i1;
        std::unique_ptr<RestFrames::InvisibleRecoFrame> rf_i2;

        //Groups
        std::unique_ptr<RestFrames::InvisibleGroup> group_inv;
        std::unique_ptr<RestFrames::CombinatoricGroup> group_comb_vis;

        //Jigsaws
        std::unique_ptr<RestFrames::SetMassInvJigsaw> jigsaw_invmass;
        std::unique_ptr<RestFrames::SetRapidityInvJigsaw> jigsaw_rapidity;
        std::unique_ptr<RestFrames::ContraBoostInvJigsaw> jigsaw_contraboost;
        std::unique_ptr<RestFrames::MinMassesCombJigsaw> jigsaw_minmass;

}; // class CalculatorTTMET2LW

} // namespace jigsaw

#endif
