#ifndef JIGSAW_CALC_H
#define JIGSAW_CALC_H

//std/stl
#include <string>
#include <vector>
#include <map>
#include <memory>

//ROOT
#include "TLorentzVector.h"

//jigsaw
#include "jigsawcalculator/JigsawCalculatorImp.h"

namespace jigsaw
{

class JigsawCalculator
{
    public :
        JigsawCalculator();
        bool initialize(std::string jigsaw_type);

        std::string jigsaw_type();

        bool load_event(std::map<std::string, std::vector<TLorentzVector> > object_map);
        std::map< std::string, float> variables();
        std::vector<std::string> variable_names();

    private :
        std::string m_jigsaw_type;
        std::unique_ptr<jigsaw::JigsawCalculatorImp> m_imp;

}; // class JigsawCalculator

} // namespace jigsaw

#endif
