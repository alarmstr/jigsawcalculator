//std/stl
#include <iostream>
using namespace std;

//ROOT
#include "TLorentzVector.h"
#include "TVectorD.h"

//RestFrames
#include "RestFrames/RestFrames.hh"
using namespace RestFrames;

int main()
{
    cout << "---------------" << endl;
    cout << "\nJigsaw Test\n" << endl;

    // declare the rest frames
    LabRecoFrame lab("lab", "lab");
    DecayRecoFrame ss("ss", "ss");
    DecayRecoFrame s1("s1", "s1");
    DecayRecoFrame s2("s2", "s2");
    VisibleRecoFrame v1("v1", "v1");
    VisibleRecoFrame v2("v2", "v2");
    InvisibleRecoFrame i1("i1", "i1");
    InvisibleRecoFrame i2("i2", "i2");

    // connect the rest frames
    lab.SetChildFrame(ss);
    ss.AddChildFrame(s1);
    ss.AddChildFrame(s2);
    s1.AddChildFrame(v1);
    s1.AddChildFrame(i1);
    s2.AddChildFrame(v2);
    s2.AddChildFrame(i2);

    // check that the connections between the rest frames are valid
    if(!lab.InitializeTree())
    {
        cout << "jigsaw_test    RestFrames::InitializeTree failure" << endl;
        return 1;
    }

    // define the groups and jigsaws
    InvisibleGroup inv("inv", "Invisible Group Jigsaws");
    inv.AddFrame(i1);
    inv.AddFrame(i2);

    CombinatoricGroup vis("vis", "Visible Group Jigsaws");
    vis.AddFrame(v1);
    vis.SetNElementsForFrame(v1, 1, false);
    vis.AddFrame(v2);
    vis.SetNElementsForFrame(v2, 1, false);

    SetMassInvJigsaw MinMassJigsaw("MinMass", "Invisible System Mass Jigsaw");
    inv.AddJigsaw(MinMassJigsaw);

    SetRapidityInvJigsaw RapidityJigsaw("RapidityJigsaw", "Invisible System Rapidity Jigsaw");
    inv.AddJigsaw(RapidityJigsaw);
    RapidityJigsaw.AddVisibleFrames(lab.GetListVisibleFrames());

    ContraBoostInvJigsaw ContraBoostJigsaw("ContraBoostJigsaw", "ContraBoost Invariant Jigsaw");
    inv.AddJigsaw(ContraBoostJigsaw);
    ContraBoostJigsaw.AddVisibleFrames( (s1.GetListVisibleFrames()), 0 );
    ContraBoostJigsaw.AddVisibleFrames( (s2.GetListVisibleFrames()), 1 );
    ContraBoostJigsaw.AddInvisibleFrame(i1, 0);
    ContraBoostJigsaw.AddInvisibleFrame(i2, 1);

    MinMassesCombJigsaw HemiJigsaw("HemiJigsaw", "Minimize m_{v_{1,2}} Jigsaw");
    vis.AddJigsaw(HemiJigsaw);
    HemiJigsaw.AddFrame(v1, 0);
    HemiJigsaw.AddFrame(v2, 1);

    // check that the Jigsaw rules are valid and set up properly
    if(!lab.InitializeAnalysis())
    {
        cout << "jigsaw_test    RestFrames::InitializeAnalysis failure" << endl;
        return 1;
    }

    // prepare the rest frames for a new event
    lab.ClearEvent();

    // set the met
    TVector3 met(5.0, 34.0, 0.);
    TLorentzVector lepton0;
    TLorentzVector lepton1;

    lepton0.SetPtEtaPhiM(45., 1.2, 0, 45.);
    lepton1.SetPtEtaPhiM(38., -2.1, 1.3, 38.);

    // propagate the observables to the restframes and jigsaws
    inv.SetLabFrameThreeVector(met);
    vis.AddLabFrameFourVector(lepton0);
    vis.AddLabFrameFourVector(lepton1);

    // run the jigsaw optimization and rules to fill the restframes
    lab.AnalyzeEvent();

    // MDeltaR
    float m_delta_r = 2.0 * v1.GetEnergy(s1);
    cout << "jigsaw_test    Calculated event MDR = " << m_delta_r << endl;

    cout << "\nDone." << endl;
    cout << "---------------" << endl;

    return 0;
}
